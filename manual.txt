 *********************************************************************
 *                                                                   *
 *  ampr-ripd.c - AMPR 44net RIPv2 Listner and Route Injector v2.4.1 *
 *                                                                   *
 *********************************************************************

 Author: Marius Petrescu, YO2LOJ, <marius@yo2loj.ro>


 Usage: ampr-ripd [-?|-h] [-d] [-v] [-s] [-r] [-i <interface>] [-a <ip|hostname|subnet>[,<ip|hostname|subnet>...]] [-p <password>]
                           [-m <metric>] [-w <window>] [-f <interface>] [-e <ip>] [-x <system command>] [-g <gateway>]

 Options:
          -?, -h                Usage info
          -d                    Debug mode: no daemonization, verbose output
          -v                    More verbose debug output
                                Using this option without debug leaves the console attached
          -s                    Save routes to /var/lib/ampr-ripd/encap.txt (encap format),
                                if this file exists, it will be loaded on startup regardless
                                of this option
          -r                    Compatibility only (ignored, raw sockets are always used)
          -i <interface>        Tunnel interface to use, defaults to tunl0
          -t <table>            Routing table to use, defaults to 'main'
          -a  <ip>[,<ip>...]    Comma separated list of IPs, hostnames or ampr subnets to be ignored.
                                Subnets can be in full network/mask (e.g. 44.182.20.0/24) or encap (e.g. 44.182.20/24) format,
                                but MUST match an entry in the RIP broadcast
                                If a hostname is used, its IP will be re-resolved 30 sec after every RIP broadcast.
                                The List contains local interface IPs by default
          -m <metric>           Metric to be used when setting routes.
                                This is a numerical value from 0 to 255. Defaults to 0.
          -w <window>           Sets the TCP window size to a given value (defaults to 840).
                                This is needed since IP over AX.25 connections have small TCP window size.
                                A value of 0 diables setting the window size (not recommended, default value should be ok)
          -p <password>         RIPv2 password, defaults to the current valid password
          -f <interface>        Interface for RIP forwarding, defaults to none/disabled
          -e <ip>               RIP forward destination IP, defaults to 224.0.0.9 if enabled
          -F <interface>        Interface for AMPR RIP forwarding, defaults to none/disabled
          -E <ip>               AMPR RIP forward destination IP, defaults to 224.0.0.9 if enabled
          -x <system command>   Execute this system command after route set/change
                                If the command includes white spaces, use quotes.
          -g <gateway>          Gateway for direct 44net connections (gateway IP or interface name).
                                If not set, the default gateway will be autodetected.
          -L <callsign@locator> Enable to sending of a callsign@locator string (actually any string) to
                                my home gateway (udp:44.182.21.1:59001).
                                The string will be sent at starup, and then every 5 minutes.
                                On termination, a "shutdown" string will be sent.
                                On HUP, the string will be sent immediately and the cycle restarted.
                                The purpose of this option is to allow the construction of a dynamic map of
                                all ampr-ripd gateways for visual display.
                                if the parameter is not set, no data will be sent.


 Observation: All routes are created with netlink protocol 44 for easy management


 Signal handling:

   - On signal SIG_HUP, ampr-ripd will delete all set routes, but will remain active,
     so routes will be set again on next received RIPv2 set, and the saved encap file updated.
   - On exit (SIG_TERM), ampr-ripd will delete all set routes and exit.

 Debug:

   - As set up in the makefile, wiyh the default debug level, using the -d option,
     the daemon will stay in the forground and allow you to find the RIPv2 password
     (see the find_pass.sh shell script).
   - To have full debug output, it has to be compiled with -D HAVE_DEBUG  or by
     uncommenting the right DOPT line.


 ************************************************************************
 * This program is free software; you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.            *
 ************************************************************************

 * Version History
 * --------------------------------------------------------------------
 *    0.9     14.Apr.2013      Alpha version, based on Hessus's rip44d
 *    1.0      1.Aug.2013      First functional version, no tables, no TCP window setting
 *    1.1      1.Aug.2013      Fully functional version
 *    1.2      3.Aug.2013      Added option for using raw sockets instead of multicast
 *    1.3      7.Aug.2013      Minor bug fix, removed compiler warnings
 *    1.4      8.Aug,2013      Possible buffer overflow fixed
 *                             Reject metrics 15 packets fixed
 *    1.5     10.Aug.2013      Corrected a stupid netmask calculation error introduced in v1.4
 *    1.6     10.Oct.2013      Changed multicast setup procedures to be interface specific (Tnx. Rob, PE1CHL)
 *    1.7      8.Feb.2014      Added support for dynamic hostnames and ampr subnets in the exclude list
 *    1.8     11.Feb.2014      Added option for route metric setting
 *    1.9     13.Feb.2014      Added window size setting option and console detaching on daemon startup
 *    1.10    14.Feb.2014      Small fixes on option and signal processing (Tnx. Demetre, SV1UY))
 *                             Using daemon() instead of fork().
 *                             Option -v without debug keeps console attached
 *    1.11    17.Feb.2014      Changed netlink route handling to overwrite/delete only routes written by ampr-ripd
 *    1.12    16.Nov.2014      Added the execution of a system command after route setting/change. This is done
 *                             on startup with encap file present and 30 seconds after RIP update if encap changes.
 *                             (Tnx. Rob, PE1CHL for the idea)
 *    1.13    20.Nov.2014      Ignore subnets for which the gateway is inside their own subnet
 *                             Reconstruct forwarded RIP messages to be able to send them even on ampr-gw outages
 *                             Forwarded RIP messages do not use authentication anymore
 *                             Forwarded RIP messages are sent 30 seconds after RIP update, otherwise every 29 seconds
 *    1.14    21.Sep.2016      Password is included in the daemon. Only need to set it sould it ever change
 *                             (OK from Brian Kantor - Tnx.)
 *                             Added man page courtesy of Ana C. Custura and the DebianHams
 *    1.15    21.Sep.2016      Removed multicast access mode, now only raw sockets are used
 *    1.16     3.Apr.2017      Added support for BGP announced 44net endpoints
 *    1.16.1   4.Apr.2017      Added SIGINT handler for CTRL-C in debug mode to clean routes on exit
 *    1.16.2   5.Apr.2017      Reopen forward socket on RIP send fail
 *    1.16.4  19.Apr.2017      Correction for hostroutes on big endian machines
 *    2.0     20.May.2017      Brought -r option back to life
 *                             Added -F and -E options for raw AMPR-RIPD forwarding
 *                             Added setting of 44.0.0.1/32 route according to RIP data
 *    2.1     28.May.2017      Fixed a segfault if using the -F option
 *                             Added the possibility to use interface names for the -g option
 *                             Interface used for raw RIP forward restarts on interface error
 *    2.1.1   29.May.2017      Corrected a tunnel interface detection error
 *    2.2     30.May.2017      Enable sending of call@locator to my gateway server for dynamic map creation
 *    2.3      4.Jun.2017      Force the use of tunnel address as source IP on call home
 *    2.4     15.Aug.2017      Corrected a segfault in NL handling on some systems (Tnx. Steve, VK5ASF & Bent, OZ6BL)
 *                             Corrected a segfault on SIGHUP if call home not set (Tnx. Steve, VK5ASF & Bent, OZ6BL)
 *                             Improved command line handling
 *    2.4.1    8.Dec.2021      Corrected a bug in netlink debug code (tnx. Daniele, IU5HKX)
 *                             It should not impact regular compiles without NL_DEBUG.

 INSTALLATION
 ------------
 -Compile with: gcc -O2 -o ampr-ripd ampr-ripd.c or use the make script
 -Copy the file ampr-ripd to a proper location, e.g. /usr/sbin
 -Create the folder /var/lib/ampr-ripd
 -Launch with proper parameters as described above
Or:
 -Just do a 'make install' in the source folder,


 KNOWN ISSUES
 ------------
 - When using other table than 'main', interogating the routes via netlink does not work properly.
   This means that on any update, the route is deleted and then recreated, even if it is already set correctly.
   This should be no problem since this happen only at startup on encap loading and on route change.
 - For BGP announced 44net endpoints, only a single subnet may be assigned to a given endpoint.
